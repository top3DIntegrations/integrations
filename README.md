## **Integration_projects** - Основная папка с проектами

### !!! Пока все тяжелые файлы по проекту лежат тут: [Google Drive](https://drive.google.com/drive/folders/1pqBWczg7N66uBWjUa1zwX5xHBQFeepM1?usp=sharing)
Папки называются и имеют структуру такую, как в GitLab

---
# Базовый файл контактов контроллера робота HCR-5
![](hcr_pinmap_default.png)
