#IR_demo_1.0
### История изменений:
V1.0 Добавлено:
* Основное описание проекта
* Первоначальный список элементов ячейки
* Добавлен проект WorkVisual для стенда, с подключенной платой Kuka IOB-16-16
* Первая версия программы, настроеная для текущей конфигурации стенда




###Файл программы V1.0:
Управление конвеером роботом. Происходит взятие объекта с стола, перенос объекта до конвейера, ожидание перемещения по конвейеру, взятие с конвейера, возвращение в целевую точку, анимация с захватом.

### WorkVisual:
На шине X44 добавлена плата Input Output Board 16
Произведен маппинг Входов и выходов с платой Kuka IOB-16-16, 
диапазон адресов DI1-DI16 DO1-DO16.
Маппинг выходов пневмоклапанов **НЕ НАСТРОЕН**
Для Сборки проект **требует** опцию **DiagnosisSafety.kop**




##Описание проекта:
Проект демо стенда для МГУТУ Разумовского. Задача перемещение по миниатюрному конвейеру упаковок с йогуртом (или иным пищевым продуктом в упаковке).

###Вид стенда: 
![](Ir_demo_front.png)
![](Ir_demo_rightside.png)
![](Ir_demo_top.png)

## Список компонентов версии:
1. Kuka kr10-1100
2. Конвеерная лента doBot Magishan 1ш
3. Двухпальцевый захват Gimatic MPRJ 1шт
4. Кастомный плк на базе Ардуино 1шт
5. блок питания 12в 1шт
6. блок питания 24в 1шт.
7. интерфейсная плата Kuka IOB 16-16 1шт.
8. компактный переключатель 1шт

## Схема подключения перефирии:
DI-3 - Входной сигнал с ардуино о статусе конвейера
DO-0 - Управление захватом, HIGH-раскрыт LOW-закрыт
DO-4 - Управление конвейером, HIGH Включен, LOW- Выключен
+24В и 0V прямое соединение с блока питания
![](kuka_x12_IOB.png)







