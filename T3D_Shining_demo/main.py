import hcrRobot
import runScript
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton
from multiprocessing import Process,Queue
import threading
from PyQt5 import QtCore
import sys

class Example(QMainWindow):
    def __init__(self):
        super().__init__()
        self.robot = hcrRobot.Robot('192.168.10.1', 502)
        self.marker = 0
        self.stop_marker = Queue()

        self.initUI()


    def initUI(self):

        self.setGeometry(1920-320, 420, 320, 60)
        self.setWindowTitle('controller')

        self.button_start = QPushButton('Start',self)
        self.button_start.resize(100,50)
        self.button_start.clicked.connect(self.ButtonStart)

        self.button_stop = QPushButton('Stop', self)
        self.button_stop.resize(100, 50)
        self.button_stop.clicked.connect(self.ButtonStop)
        self.button_stop.move(110,0)

        self.button_restart = QPushButton('Restart', self)
        self.button_restart.resize(100, 50)
        self.button_restart.clicked.connect(self.ButtonReset)
        self.button_restart.move(220, 0)

        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.show()

    def ButtonStart(self):
        self.marker = 1
        self.controller = threading.Thread(target=runScript.Worker, args=(self.robot, self.stop_marker))
        self.controller.start()
        print ("started")

    def ButtonStop(self):
        self.stop_marker.put(2)
        #self.controller.stop()
        #self.controller.join()

    def ButtonReset(self):
        self.marker = False
        self.robot.MovePTP({'j1': 141.63, 'j2': -74.27, 'j3': -119.59,
           'j4': -170.59, 'j5': -40.53, 'j6': 317.3})

    def Worker(self, stop_marker ):
        print('Robot start')
        self.robot.SetLinSpeed(80)
        # while True:
        #     if not stop_marker.empty():
        #         a = stop_marker.get()
        #         break
        self.robot.MoveLin({
                'x': -156.63, 'y': 397.59, 'z': 319.19,
                'rx': 94.15, 'ry': 46, 'rz': -174.92})
        self.robot.MoveLin({
                'x': -156.63, 'y': 466.33, 'z': 594.92,
                'rx': 123.35, 'ry': 40.09, 'rz': -154.73})
        self.robot.MoveLin({
                'x': -156.63, 'y': 397.59, 'z': 319.19,
                'rx': 94.15, 'ry': 46, 'rz': -174.92})
        self.robot.MoveLin({
                'x': -156.63, 'y': 466.33, 'z': 594.92,
                'rx': 123.35, 'ry': 40.09, 'rz': -154.73})


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
