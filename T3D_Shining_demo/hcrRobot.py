from multiprocessing import Process, Pipe ,Queue
import robot_controller
import time

'''
    command = {
                'x': 0, 'y': 0, 'z': 0,
                'rx': 0, 'ry': 0, 'rz': 0,
                'acc':0, 'vel':0
               } 
    
'''
class Robot():
    def __init__ (self,ip,port):
        #self.robot_port = ModBus_Communication.connect(ip)
        self.move_command_ready = 0
        self.move_command_type = 0
        self.move_command = {
                'x': 0, 'y': 0, 'z': 0,
                'rx': 0, 'ry': 0, 'rz': 0
                
               } 
        self.current_pose = {
                'x': 0, 'y': 0, 'z': 0,
                'rx': 0, 'ry': 0, 'rz': 0}
        self.pipe_marker = Queue()
        self.data_marker = Queue()
        self.pipe_send_father,self.pipe_send_child = Pipe()
        self.controller = Process(target = robot_controller.Controller, args=(ip,port,self.pipe_marker, self.data_marker, self.pipe_send_child,))
        self.controller.start()
        #self.controller.daemon =True
        
         
    
    
    def SetLinSpeed(self,speed):
        self.pipe_send_father.send([7,speed])
        self.pipe_marker.put(2)
        
    def SetJointSpeed(self,speed):
        self.pipe_send_father.send([6,speed])
        self.pipe_marker.put(2)
        
    def SetAcc(self,acceleration):
        self.pipe_send_father.send([8,acceleration])
        self.pipe_marker.put(2)
    
    
    def __del__(self):
        print('del run')
        self.controller.terminate()
        self.controller.join()
    
    def Disconnect(self):
        print('disconnected')
        self.controller.terminate()
        self.controller.join()
    
    def MovePTP(self,move_command):
                #print ('move sended')
        self.pipe_send_father.send([2,2,move_command])
        self.pipe_marker.put(1)
        while True:
            if not self.data_marker.empty():
                tmp = self.data_marker.get()
                #print('move marker recieved')
                break
    
    def MovePTPRel(self,move_command):
        #start = time.time()
        current = self.GetJointPose()
        #end = time.time()
        #print('Get pose time',end-start,' sec') 
        #print ('old Current',current)
        for key in list(move_command.keys()):
            current[key]+=move_command[key]
        #print ('new Current',current)
        start = time.time()
        self.pipe_send_father.send([2,2,current])
        self.pipe_marker.put(1)
        while True:
            if not self.data_marker.empty():
                tmp = self.data_marker.get()
                #print('move marker recieved')
                break
        end = time.time()
        print('Move time',end-start,' sec') 
    
    def GetJointPose(self):
        self.pipe_send_father.send([4,0,self.move_command])
        self.pipe_marker.put(1)
        self.move_command = {
                'x': 0, 'y': 0, 'z': 0,
                'rx': 0, 'ry': 0, 'rz': 0
                
               } 
        
        while True:
            if not self.data_marker.empty():
                self.data_marker.get()
                #print('marker here get data')
                inc = self.pipe_send_father.recv()
                #print ('inc is ',inc)
                break
            #else:
            #    print ('no marker')
        #print('While ended')
        ret = {'j1':inc['x'],'j2':inc['y'],'j3':inc['z'],
               'j4':inc['rx'],'j5':inc['ry'],'j6':inc['rz']}
        return ret
        
        
    def GetPose(self):
        '''    
                Sends request for current Decart coodrinates, and whaits for robots response
        '''
        self.pipe_send_father.send([5,0])
        self.pipe_marker.put(2)
        while True:
            if not self.data_marker.empty():
                self.data_marker.get()
                inc = self.pipe_send_father.recv()
                break
        return inc
    
    
    def MoveLin(self,move_command):
        #print ('move sended')
        self.pipe_send_father.send([1,2,move_command])
        self.pipe_marker.put(1)
        while True:
            if not self.data_marker.empty():
                tmp = self.data_marker.get()
                #print('move marker recieved')
                break
                 
    def MoveLinRel(self,move_command):
        start1 = time.time()
        current = self.GetPose()
        end2 = time.time()
        #print('Get pose time',end-start,' sec') 
        #print ('old Current',current)
        for key in list(move_command.keys()):
            current[key]+=move_command[key]
        #print ('new Current',current)
        #start = time.time()
        self.pipe_send_father.send([1,2,current])
        self.pipe_marker.put(1)
        while True:
            if not self.data_marker.empty():
                tmp = self.data_marker.get()
                #print('move marker recieved')
                break
        #end = time.time()
        #print('Move time',end-start,' sec| getPose time',end2-start1) 
      
    def GetGedDInput(self,inp):  
        if input>7:
            inp = 7
        self.pipe_send_father.send([12,inp*1000])
        self.pipe_marker.put(2)
        while True:
            if not self.data_marker.empty():
                self.data_marker.get()
                inc = self.pipe_send_father.recv()
                break
        state = inc%10
        inp = (inc-state)/1000
        return ((inp,state))
    
    def GetRedDInput(self,inp):  
        if inp>7:
            inp = 7
        self.pipe_send_father.send([12,(inp+8)*1000])
        self.pipe_marker.put(2)
        while True:
            if not self.data_marker.empty():
                self.data_marker.get()
                inc = self.pipe_send_father.recv()
                break
        state = inc%10
        inp = (inc-state)/1000
        return ((input,state))
    
    def GetToolDInput(self,inp):  
        if inp>3:
            inp = 3
        self.pipe_send_father.send([12,(inp+16)*1000])
        self.pipe_marker.put(2)
        while True:
            if not self.data_marker.empty():
                self.data_marker.get()
                inc = self.pipe_send_father.recv()
                break
        state = inc%10
        inp = (inc-state)/1000
        return ((inp,state))
               
    
    def GetGedDOutput(self,inp):  
        if input>7:
            inp = 7
        self.pipe_send_father.send([11,inp*1000])
        self.pipe_marker.put(2)
        while True:
            if not self.data_marker.empty():
                self.data_marker.get()
                inc = self.pipe_send_father.recv()
                break
        state = inc%10
        inp = (inc-state)/1000
        return ((inp,state))
    
    def GetRedDOutput(self,inp):  
        if inp>7:
            inp = 7
        self.pipe_send_father.send([11,(inp+8)*1000])
        self.pipe_marker.put(2)
        while True:
            if not self.data_marker.empty():
                self.data_marker.get()
                inc = self.pipe_send_father.recv()
                break
        state = inc%10
        inp = (inc-state)/1000
        return ((input,state))
    
    def GetToolDoutput(self,inp):  
        if inp>3:
            inp = 3
        self.pipe_send_father.send([11,(inp+16)*1000])
        self.pipe_marker.put(2)
        while True:
            if not self.data_marker.empty():
                self.data_marker.get()
                inc = self.pipe_send_father.recv()
                break
        state = inc%10
        inp = (inc-state)/1000
        return ((inp,state))
                              
    def SetGenDOutput(self,output,state):
        #print('set output ',output,' with state ',state)
        if output>7:
            output = 7
        self.pipe_send_father.send([10,output*1000+state])
        self.pipe_marker.put(2)
        time.sleep(0.5)
        
    
    def SetRedDOutput(self,output,state):
        #print('set Redaunt output ',output,' with state ',state)
        if output>7:
            output = 7
        self.pipe_send_father.send([10,(output+8)*1000+state])
        self.pipe_marker.put(2)
        time.sleep(0.3)
    
    def SetToolDOutput(self,output,state):
        print('set Tooloutput ',output,' with state ',state)
        if output>3:
            output = 3
        self.pipe_send_father.send([10,(output+16)*1000+state])
        self.pipe_marker.put(2)
        time.sleep(0.5)
    
    def SetToolPower(self,voltage):
        availiable =(0,12,24)
        if voltage not in availiable:
            voltage = 0
        self.pipe_send_father.send([9,voltage])
        self.pipe_marker.put(2)
        time.sleep(0.5)
    
if  __name__ == '__main__':
    robot = Robot('192.168.10.1',502)
    time.sleep(3)
    '''
    for i in range(20):
        print(robot.GetGedDInput(1))
        time.sleep(3)
    
    robot.SetGenDOutput(1, 1)
    time.sleep(3)
    robot.SetGenDOutput(0, 1)
    time.sleep(3)
    robot.SetGenDOutput(1, 0)
    time.sleep(3)
    robot.SetGenDOutput(0, 0)
    time.sleep(3)
    robot.SetRedDOutput(3, 0)
    time.sleep(3)
    robot.SetRedDOutput(4, 1)
    time.sleep(3)
    robot.SetToolDOutput(1, 0)
    time.sleep(3)
    robot.SetToolDOutput(0, 1)
    '''
    
    '''
    s = robot.GetPose()
    print("s is",s)
    s['x']+=100
    print('new s',s)
    relMove = {'x':10}
    joint = robot.GetJointPose()
    print(joint)
    #robot.SetJointSpeed(50)
    joint['j1']+=40
    print('joint sended',joint)
    robot.MovePTPRel({'j1':40})
    joint['j1']-=40
    robot.MovePTP(joint)
    '''
    
    for i in range(5):
        robot.SetLinSpeed(40*i+20)
        time.sleep(2)
        #print ('new speed',20*i+20)
        robot.MoveLinRel({'x':50})
        robot.MoveLinRel({'y':-50})
        robot.MoveLinRel({'x':-50})
        robot.MoveLinRel({'y':+50})
    '''
    robot.SetLinSpeed(30)
    time.sleep(3)
    robot.MoveLin(s)
    #time.sleep(3)
    robot.SetLinSpeed(100)
    s['x']-=100
    time.sleep(3)
    robot.MoveLin(s)
    robot.SetJointSpeed(50)
    print ('script done')
    #robot.Disconnect()
    
    #print('current pose', robot.GetPose())
    '''
    